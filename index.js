//[SECTION] Objects

// An object is a data type that is used to represent real world objects.
// Sub section Creating objects using object initializers/literal notation


let cellphone={
	name: 'Nokia 3210',
	manufactureDate: 1999,
	color: "Grey",
	
};

console.log('Result from creating objects using initializers/literal notion: ');
console.log(cellphone);
/*
Syntax:
	let objectName= {
		keyA: valueA,
		keyB: valueB,
	}

*/
// SUB SECTION] Creating objects using a constructor Function

	//creates a reusable function to create several objects that have the same
	// data  structure

	function Laptop(name, manufactureDate, color){
		this.name= name;
		this.manufactureDate= manufactureDate;
		this.color= color;
	};

	//This is an Object
		// the 'this' keyword allows to assign a new object's properties
		//by associating them with values receiived from a constructor
		// function's parameter

		//the 'new ' operator creates an instance of an object

	let laptop = new Laptop('Lenovo', 2008, 'Red');
	console.log('Result from creaing objects using object constructors: ');
	console.log(laptop);

	/*
	Syntax:
		function ObjectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		};
	*/

	/*class Car{
		constructor(name, manufacturer, color){
			this.name = name;
			this.manufacturer = manufacturer;
			this.color = color;
		}
	}

	let carA = new Car('Civic', 'Honda', 'White');
	let carB = new Car('Corolla', 'Toyota', 'Red');

	console.log(carA);
	console.log(carB);*/

	let myLaptop = new Laptop('MacBook Air', 2020, 'Red');
	console.log('Result from creating objects using object constructors:');
	console.log(myLaptop);

	//sub section creating an Empty Objects

	let computer={};
	let myComputer = new Object();
		console.log(computer);
		console.log(myComputer);

	//[SECTION] Accessing oBject properties
		//using dot notation

		console.log('Result from dot notation: '+ myLaptop.manufactureDate);

		//using the square bracket notation
		console.log('Result from square bracket notation: '+myLaptop['name'])


		//sub section accessing array objects
			//accessing array element can also be done using square brackets

			let array= [laptop, myLaptop];
			console.log(array[0].name);
			//differentiation between accessing array and object properties
			//this tells us that array[0] is an object be using the dot notation
			console.log(array[0]['name'])

	//[SECTION] Initializing/Adding/Deleting/Reassigning Object Properties
		//Like any other variable in Javascript, objects may have their properties
		//initialized/added after the object was created.

			let car= {};

			//[sub section] initializing/adding object properties
			//using dot notation

			car.name= 'Honda Civic';
			console.log('Result from adding properties using dot notation:');
			console.log(car);

			//sub section- initializing adding object properties using bracket notation

			car['manufacture date'] = 2019
			console.log(car['manufacture date']);
			console.log(car['Manufacture date']);//undefined cuz capitalM
			//console.log(car.manufacture date);//error cuz w space usin dot

			console.log(car);

			//sub section Deleting Object Properties

			delete car['manufacture date'];
			console.log('Result from deleting properties')
			console.log(car);

			//sub-section Reassigning object properties

				car.name= 'Dodge Charger R/T';
				console.log('Result from reassigning properties:')
				console.log(car);

	//SECTION Object Methods
		// a method is  a function which is a property of an object

		let person= {
			name:'John',
			talk: function(){
				console.log('Hello my name is '+ this.name);
			}
		};

		console.log(person);
		console.log('Result from object methods: ');
		person.talk();

		//subsection adding methods to objects

		person.walk = function(){
			console.log(this.name + ' walked 25 steps forward')
		};
		person.walk();

		//methods are useful for creating reusable function that perform
		// task related to objects

		let friend= {
			firstName: 'Joe',
			lastName: 'Smith',
			address: {
				city: 'Austin',
				country: 'Texas'
			},
			emails: ['joe@mail.com', 'joesmith@email.xyz'],
			introduce: function(){
				console.log('Hello my name is '+this.firstName+ ' '+ this.lastName+" & I'am from "+this.address.city+ ' '+this.address.country+'!' +' My Email is: '+friend.emails[1]);
			}
		};
		console.log(friend);
		friend.introduce();

	//SECTION real world application of objects
	/*
		Scenario
			1. We wud like to cr8 a game that would have several pokemon
				interact with each other.
			2. Every pokemon would have the same set of stats, properties
				and functions
	*/

	let myPokemon={
		name:'Pikachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log('This Pokemon tackled targetPokemon')
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		},
		faint: function(){
			console.log('Pokemon fainted');
		}
	}

	console.log(myPokemon);

	function Pokemon(name, level){
		//Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;

		//Methods
		this.tackle= function(target){
			console.log(this.name+ ' tackled '+ target.name);
			console.log("targetPokemon's health is now reduced to "+ Number(target.health - this.attack));

		};

		this.faint= function(){
			console.log(this.name+ ' fainted. ');

		};
	}
	//create a new instances of the 'pokemon' object each wit their 
			//unioque properties

			let pikachu= new Pokemon ('Pikachu', 16);
			let rattata= new Pokemon ('Ratta', 8);

			pikachu.tackle(rattata);
				rattata.faint()